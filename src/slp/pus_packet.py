from .exceptions import DecodingError
from .space_packet import SpacePacketPrimaryHeader,\
    SPACE_PACKET_PRIMARY_HEADER_SIZE


PECF_SIZE = 2
PUS_DATA_FIELD_MIN_SIZE = 3


class PusDataFieldHeader:

    def __init__(
        self,
        pus_version,
        time_reference,
        service_type,
        service_subtype,
        message_counter=None,
        destination_id=None,
        time=None,
        message_counter_size=0,
        destination_id_size=0,
        time_size=0,
        spare_field_size=0
    ):
        self.pus_version = pus_version
        self.time_reference = time_reference
        self.service_type = service_type
        self.service_subtype = service_subtype
        self.message_counter = message_counter
        self.destination_id = destination_id
        self.time = time
        self.message_counter_size = message_counter_size
        self.destination_id_size = destination_id_size
        self.time_size = time_size
        self.spare_field_size = spare_field_size

    def __len__(self):
        return PUS_DATA_FIELD_MIN_SIZE\
            + self.message_counter_size\
            + self.destination_id_size\
            + self.time_size\
            + self.spare_field_size

    def encode(self):
        data = bytearray()
        ## TODO...
        return data

    @classmethod
    def decode(
            cls,
            data,
            message_counter_size=0,
            destination_id_size=0,
            time_size=0,
            spare_field_size=0
    ):
        if len(data) < PUS_DATA_FIELD_MIN_SIZE:
            raise DecodingError

        pus_version = data[0] >> 4

        if pus_version == 0:
            raise DecodingError("PUS Version 0 not implemented")

        elif pus_version == 1:  # PUS-A
            time_reference = None
            service_type = data[1]
            service_subtype = data[2]

            if message_counter_size:
                message_counter = data[3:3 + message_counter_size]
                data = data[3 + message_counter_size:0]
            else:
                message_counter = None
                data = data[3:]

            if destination_id_size:
                destination_id = data[0:destination_id_size]
                data = data[destination_id_size:]
            else:
                destination_id = None

            if time_size:
                time = data[:time_size]
            else:
                time = None

        elif pus_version == 2:  # PUS-C
            time_reference = data[0] & 0x0F
            service_type = data[1]
            service_subtype = data[2]
            message_counter = data[3] << 8 | data[4]
            destination_id = data[5] << 8 | data[6]
            # TODO:
            # time =
            raise NotImplementedError

        else:
            raise DecodingError

        return cls(
            pus_version=pus_version,
            time_reference=time_reference,
            service_type=service_type,
            service_subtype=service_subtype,
            message_counter=message_counter,
            destination_id=destination_id,
            time=time,
            spare_field_size=spare_field_size
        )


class PusPacket:

    def __init__(
        self,
        primary_header,
        secondary_header,
        source_data,
        packet_error_control_field
    ):
        self.primary_header = primary_header
        self.secondary_header = secondary_header
        self.source_data = source_data
        self.packet_error_control_field = packet_error_control_field

    def encode(self):
        data = bytearray()
        data.append(self.primary_header.encode())
        data.append(self.secondary_header.encode())
        data.append(self.source_data)
        data.append(self.packet_error_control)
        return data

    @classmethod
    def decode(
        cls,
        data,
        has_pecf,
        message_counter_size=0,
        destination_id_size=0,
        time_size=0,
        spare_field_size=0
    ):
        primary_header = SpacePacketPrimaryHeader.decode(data)
        # strip away packet header
        data = data[SPACE_PACKET_PRIMARY_HEADER_SIZE:]

        secondary_header = PusDataFieldHeader.decode(
            data,
            message_counter_size,
            destination_id_size,
            time_size,
            spare_field_size,
        )
        # strip away data field header
        data = data[len(secondary_header):]

        if has_pecf:
            source_data, packet_error_control_field =\
                data[:-PECF_SIZE], data[-PECF_SIZE:]
        else:
            source_data = data
            packet_error_control_field = None

        return cls(
            primary_header=primary_header,
            secondary_header=secondary_header,
            source_data=source_data,
            packet_error_control_field=packet_error_control_field
        )
