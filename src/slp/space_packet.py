import enum

from .exceptions import DecodingError


SPACE_PACKET_PRIMARY_HEADER_SIZE = 6
APID_IDLE_PACKET = 0b11111111111


class PacketType(enum.IntEnum):
    TM = 0
    TC = 1


class SequenceFlags(enum.IntEnum):
    CONTINUATION_SEGMENT = 0b00
    FIRST_SEGMENT = 0b01
    LAST_SEGMENT = 0b10
    UNSEGMENTED = 0b11


class SpacePacketPrimaryHeader:

    def __init__(
        self,
        packet_type,
        secondary_header_flag,
        apid,
        sequence_flags,
        packet_sequence_count,
        packet_data_length,
        packet_version=0b000,
    ):
        self.packet_type = packet_type
        self.secondary_header_flag = secondary_header_flag
        self.apid = apid
        self.sequence_flags = sequence_flags
        self.packet_sequence_count = packet_sequence_count
        self.packet_data_length = packet_data_length
        self.packet_version = packet_version

    def __len__(self):
        return SPACE_PACKET_PRIMARY_HEADER_SIZE

    @property
    def packet_len(self):
        return SPACE_PACKET_PRIMARY_HEADER_SIZE + (self.packet_data_length + 1)

    def encode(self):
        data = bytearray()

        packet_id = (
            self.packet_type << 12 |
            int(self.secondary_header_flag) << 11 |
            self.apid) & 0x1FFF
        data.append((self.packet_version << 5) | (packet_id & 0xFF00) >> 8)
        data.append(packet_id & 0xFF)

        packet_sequence_control =\
            (self.packet_sequence_count & 0x3FFF) | (self.sequence_flags << 14)
        data.append((packet_sequence_control & 0xFF00) >> 8)
        data.append(packet_sequence_control & 0xFF)

        data.append((self.data_length & 0xFF00) >> 8)
        data.append(self.data_length & 0xFF)

        return data

    @classmethod
    def decode(cls, data):
        if len(data) < SPACE_PACKET_PRIMARY_HEADER_SIZE:
            raise DecodingError

        packet_version = data[0] >> 5

        packet_type = (data[0] & 0x10) >> 4
        packet_type = PacketType(packet_type)

        secondary_header_flag = (data[0] & 0x8) >> 3
        secondary_header_flag = bool(secondary_header_flag)
        apid = ((data[0] & 0x07) << 8) | data[1]

        sequence_flags = (data[2] & 0xC0) >> 6
        sequence_flags = SequenceFlags(sequence_flags)

        packet_sequence_count = ((data[2] << 8) | data[3]) & 0x3FFF
        packet_data_length = data[4] << 8 | data[5]

        return cls(
            packet_type=packet_type,
            secondary_header_flag=secondary_header_flag,
            apid=apid,
            sequence_flags=sequence_flags,
            packet_sequence_count=packet_sequence_count,
            packet_data_length=packet_data_length,
            packet_version=packet_version,
        )
