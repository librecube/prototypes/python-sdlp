from .exceptions import DecodingError


CLCW_SIZE = 4


class CommunicationsLinkControlWord:

    def __init__(
        self,
        status_field,
        cop_in_effect,
        vc,
        flag_no_rf,
        flag_no_bitlock,
        flag_lockout,
        flag_wait,
        flag_retransmit,
        farm_b_counter,
        report_value,
        control_word_type=0b0,
        clcw_version=0b00,
    ):
        self.status_field = status_field
        self.cop_in_effect = cop_in_effect
        self.vc = vc
        self.flag_no_rf = flag_no_rf
        self.flag_no_bitlock = flag_no_bitlock
        self.flag_lockout = flag_lockout
        self.flag_wait = flag_wait
        self.flag_retransmit = flag_retransmit
        self.farm_b_counter = farm_b_counter
        self.report_value = report_value
        self.control_word_type = control_word_type
        self.clcw_version = clcw_version

    def encode(self):
        data = bytearray()

        data.append(
            self.control_word_type << 7 |
            self.clcw_version << 5 |
            self.status_field << 3 |
            self.cop_in_effect
        )

        data.append(self.vc << 2)

        data.append(
            self.flag_no_rf << 7 |
            self.flag_no_bitlock << 6 |
            self.flag_lockout << 5 |
            self.flag_wait << 4 |
            self.flag_retransmit << 3 |
            self.farm_b_counter << 1
        )

        data.append(self.report_value)

        return data

    @classmethod
    def decode(cls, data):
        if len(data) < CLCW_SIZE:
            raise DecodingError

        control_word_type = data[0] >> 7
        clcw_version = (data[0] >> 5) & 0x03
        status_field = (data[0] >> 3) & 0x07
        cop_in_effect = data[0] & 0x03

        vc = data[1] >> 2

        flag_no_rf = bool((data[2] >> 7) & 0x01)
        flag_no_bitlock = bool((data[2] >> 6) & 0x01)
        flag_lockout = bool((data[2] >> 5) & 0x01)
        flag_wait = bool((data[2] >> 4) & 0x01)
        flag_retransmit = bool((data[2] >> 3) & 0x01)
        farm_b_counter = (data[2] >> 1) & 0x03

        report_value = data[3]

        return cls(
            status_field=status_field,
            cop_in_effect=cop_in_effect,
            vc=vc,
            flag_no_rf=flag_no_rf,
            flag_no_bitlock=flag_no_bitlock,
            flag_lockout=flag_lockout,
            flag_wait=flag_wait,
            flag_retransmit=flag_retransmit,
            farm_b_counter=farm_b_counter,
            report_value=report_value,
            control_word_type=control_word_type,
            clcw_version=clcw_version,
        )
