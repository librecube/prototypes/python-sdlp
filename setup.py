import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    readme = fh.read()

setuptools.setup(
    name='slp',
    version='0.0.1',
    description="Space Link Protocols",
    long_description=readme,
    long_description_content_type="text/markdown",
    url="https://xxxx",
    author="LibreCube",
    author_email="info@librecube.org",
    license="MIT",
    packages=setuptools.find_namespace_packages(where="src"),
    package_dir={"": "src"},
    python_requires=">=3.6",
    include_package_data=True,
)
